package com.jwt.config;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jwt.exception.ErrorCodes;
import com.jwt.exception.ErrorSection;

import io.github.bucket4j.Bucket;

@Component
public class RateLimitingFilter implements Filter {

	@Autowired
	private Bucket rateLimiter;
	
	@Autowired
	ObjectMapper mapper;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (rateLimiter.tryConsume(1)) {
			// Request is allowed, proceed with the filter chain
			chain.doFilter(request, response);
		} else {
			// Rate limit exceeded, handle accordingly (e.g., return an error response)
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			httpResponse.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
			httpResponse.setStatus(Integer.parseInt(ErrorCodes.TOO_MANY_REQUEST.getErrorCode()));
			ErrorSection error = new ErrorSection(ErrorCodes.TOO_MANY_REQUEST.getErrorCode(),
					Arrays.asList(ErrorCodes.TOO_MANY_REQUEST.getMessage()));
			response.getWriter().write(mapper.writeValueAsString(error));
		}
	}

	@Override
	public void destroy() {
		// Cleanup code, if needed
	}
}
