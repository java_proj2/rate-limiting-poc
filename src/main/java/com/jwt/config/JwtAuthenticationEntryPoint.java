package com.jwt.config;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jwt.exception.ErrorCodes;
import com.jwt.exception.ErrorSection;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

	private static final long serialVersionUID = -7858869558953243875L;

	@Autowired
	ObjectMapper mapper;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {

		authException.printStackTrace();
		
		response.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		response.setStatus(Integer.parseInt(ErrorCodes.AUTHORIZATION_FAILED.getErrorCode()));
		ErrorSection error = new ErrorSection(ErrorCodes.AUTHORIZATION_FAILED.getErrorCode(),
				Arrays.asList(authException.getMessage(), ErrorCodes.AUTHORIZATION_FAILED.getMessage()));
		response.getWriter().write(mapper.writeValueAsString(error));

	}

}
