package com.jwt.aspect;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.jwt.exception.ErrorCodes;
import com.jwt.exception.ErrorSection;
import com.jwt.exception.PocRuntimeException;

@ControllerAdvice
public class PocControllerAdvice extends ResponseEntityExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(PocControllerAdvice.class);

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<ErrorSection> handleAllUncaughtException(Exception exception, WebRequest request) {
		log.error("Unknown error occurred", exception);
		ErrorSection newErrorResponse = new ErrorSection(ErrorCodes.INTERNAL_SERVER_ERROR.getErrorCode(),
				Arrays.asList(exception.getMessage(), ErrorCodes.INTERNAL_SERVER_ERROR.getMessage()));
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(newErrorResponse);
	}

	@ExceptionHandler(value = PocRuntimeException.class)
	@ResponseBody
	ResponseEntity<ErrorSection> onException(PocRuntimeException pocRuntimeException) {
		ErrorSection newErrorResponse = newErrorResponse(pocRuntimeException);

		if (ErrorCodes.INTERNAL_SERVER_ERROR.equals(pocRuntimeException.getErrorCode())) {

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(newErrorResponse);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(newErrorResponse);
		}

	}

	private ErrorSection newErrorResponse(PocRuntimeException pocRuntimeException) {
		return new ErrorSection(pocRuntimeException.getErrorCode().getErrorCode(),
				Arrays.asList(pocRuntimeException.getMessage()));
	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception e, @Nullable Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
			request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, e, WebRequest.SCOPE_REQUEST);
		}

		if (e.getCause() instanceof UnrecognizedPropertyException) {
			body = onJsonMappingException((HttpMessageNotReadableException) e);
		}
		log.debug("Unhandled ERROR", e);
		return new ResponseEntity<>(body, headers, status);
	}

	private ErrorSection onJsonMappingException(HttpMessageNotReadableException e) {
		String substring = "not marked as ignorable";
		int index = e.getMessage().indexOf(substring);

		if (index > 0) {
			return newErrorResponse(e.getMessage().substring(0, index + substring.length()));
		} else {
			return null;
		}
	}

	private ErrorSection newErrorResponse(String substring) {
		String message = String.format(ErrorCodes.RECORD_NOT_FOUND.getMessage(),
				StringUtils.isEmpty(substring) ? "" : substring);
		return new ErrorSection(ErrorCodes.RECORD_NOT_FOUND.getErrorCode(), Arrays.asList(message));
	}
}
