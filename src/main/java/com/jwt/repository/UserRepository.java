package com.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jwt.model.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {


  /**
   * Method is to return the list of boxes of given asn no.
   * 
   * @param asnNo asn no
   * @return
   */
  UserEntity findByToken(String token);

}
