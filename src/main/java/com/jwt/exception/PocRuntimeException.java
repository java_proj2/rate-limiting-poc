package com.jwt.exception;

import org.apache.commons.lang3.StringUtils;


public class PocRuntimeException extends RuntimeException {

  private static final long serialVersionUID = 1L;
  private ErrorCodes errorCode;

  /**
   * Create exception with error code and throws cause.
   * 
   * @param errorCode error code of error.
   * @param pocError actual exception.
   */
  public PocRuntimeException(ErrorCodes errorCode, Exception pocError) {
    super(String.format(errorCode.getMessage(),
        StringUtils.isEmpty(pocError.getMessage()) ? "" : pocError.getMessage()));
    this.errorCode = errorCode;
  }

  /**
   * Create exception with error code and String message.
   * 
   * @param pocError error code of error.
   * @param message  actual message for exception.
   */
	public PocRuntimeException(ErrorCodes pocError, String message) {
    super(String.format(pocError.getMessage(), StringUtils.isEmpty(message) ? "" : message));
    this.errorCode = pocError;
  }

  /**
   * Using parent constructor to set message.
   * 
   * @param message cause of exception.
   */
  public PocRuntimeException(String message) {
    super(message);
  }

  /**
   * Error code for particular exception.
   * 
   * @return error code.
   */
  public ErrorCodes getErrorCode() {
    return errorCode;
  }

}
