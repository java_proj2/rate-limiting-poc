package com.jwt.exception;

import java.util.List;

/**
 * error section.
 * 
 * @author Administrator
 *
 */
public class ErrorSection {
	private String errorCode;

	private List<String> errorDescription;

	/**
	 * constructor.
	 * 
	 * @param errorCode        errorcode
	 * @param errorDescription errorDiscription
	 */
	public ErrorSection(String errorCode, List<String> errorDescription) {
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public List<String> getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(List<String> errorDescription) {
		this.errorDescription = errorDescription;
	}

}
