package com.jwt.exception;

public enum ErrorCodes {

  RECORD_NOT_FOUND("404", "Record not Found"), TOO_MANY_REQUEST("429",
      "Too Many Request"), AUTHORIZATION_FAILED("401",
          "Authorization Failed"), INTERNAL_SERVER_ERROR("500",
                  "INternal Server Error");
	

  private String errorCode;
  private String message;

  private ErrorCodes(String errorCode, String message) {

    this.errorCode = errorCode;
    this.message = message;
  }

  /**
   * .
   * 
   * @return a error code which is enum object.
   */
  public String getErrorCode() {
    return errorCode;
  }

  /**
   * .
   * 
   * @return the message hold by enum object
   */
  public String getMessage() {
    return message;
  }

}
