package com.jwt.exception;

import org.apache.commons.lang3.StringUtils;

public class PocException extends Exception {

	private static final long serialVersionUID = 1L;
	private ErrorCodes errorCode;

	/**
	 * Creating Exception with enum and message together.
	 * 
	 */
	public PocException(ErrorCodes errorCode, String message) {
		super(String.format(errorCode.getMessage(), StringUtils.isEmpty(message) ? "" : message));
		this.errorCode = errorCode;
	}

	/**
	 * Get the enum object.
	 * 
	 * @return
	 */
	public ErrorCodes getErrorCode() {
		return errorCode;
	}

}
