package com.jwt.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping({ "/ccp/api/team" })
public class TestTeamController {

	@GetMapping(value = "/test")
	public String hello() {
		return "This is for jwt token validation /ccp/api/team !";
	}

}
